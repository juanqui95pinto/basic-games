﻿using System;

/// <summary>
/// 1. Change the number reange from 1 to 1.000.000
/// 2. Game should ask us to guess a number
/// 3. Give a clue of the number is higher or lower than the guess
/// 4. Inform the player if he won
/// </summary>

namespace ConsolePlay.GessTheNumber
{
    class Program
    {
        static Random rnd = new Random();
        
        static void Main(string[] args)
        {
            int start = 1, end = 1000000;
            int value = makeRandomNum(start, end);
            int guess = 0;

            while (guess != value)
            {
                Console.Write("Guess the number: ");
                guess = Convert.ToInt32(Console.ReadLine());

                if (guess < value)
                {
                    Console.WriteLine("The number is Higher.");
                }
                else if(guess > value)
                {
                    Console.WriteLine("The number is Lower.");
                }
            }

            Console.WriteLine("Congratulations!!! You guess the number. You WON.");

        }

        private static int makeRandomNum(int start, int end)
        {
            Console.WriteLine("The computer choose a number betwen {0} and {1}", start, end);

            return rnd.Next(start, end);
        }
    }
}
